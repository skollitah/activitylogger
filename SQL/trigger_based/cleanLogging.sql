---------------------------------------------------------------------------------------
-- STEP0: SCRIPT CONFIGURATION 
---------------------------------------------------------------------------------------
if exists (SELECT 1 FROM tempdb..sysobjects WHERE [ID] = OBJECT_ID('tempdb..#scriptConfiguration') AND type = 'U') begin
	drop table #scriptConfiguration
end
	
create table #scriptConfiguration (
	Config_idn varchar(30)
	,Config_desc varchar(500)
	,ConfigValue varchar(2000)
)

insert into #scriptConfiguration(Config_idn, Config_desc, ConfigValue)
	select	'TableNamePostfix', '', ''
	union
	select	'DbNamePostfix', '', '_History'

select	Config_idn
	,Config_desc
	,ConfigValue
from	#scriptConfiguration
go

---------------------------------------------------------------------------------------
-- STEP2: DROP COLUMNS
---------------------------------------------------------------------------------------

declare @sql varchar(8000)
declare @name varchar(256)
declare @db varchar(256)
declare @dbp varchar(256)
declare @postfix varchar(30)
declare @tpostfix varchar(15)

set @postfix = (
	select	ConfigValue
	from	#scriptConfiguration
	where	Config_idn = 'DbNamePostfix'
	)
set @tpostfix = (
	select	ConfigValue
	from	#scriptConfiguration
	where	Config_idn = 'TableNamePostfix'
	)

set @db = db_name()
set @dbp = @db + @postfix

declare TriggerAdd cursor for
	select	distinct
		o1.[oname]
	from	dbm.db_model_get_tables_columns o1
	where 	o1.oname not in (
		select	distinct
			oname
		from	dbm.db_model_get_tables_columns
		where	alterable = 'N' and cname <> 'TableRowVersion'
	)
for read only

open TriggerAdd


while 1=1 begin
	fetch next from TriggerAdd into @name

	if @@fetch_status <> 0 begin
		break
	end

	set @name = @name + @tpostfix	
	
	set @sql = '		
		use ' + @dbp + '
		if dbm.db_model_exists_table_column(''dbo'', ''' + @name + ''', ''TableRowVersion'') = 1 begin
			alter table [' + @name + '] drop column [TableRowVersion]
		end
		
		if dbm.db_model_exists_table_column(''dbo'', ''' + @name + ''', ''ContextOperationKind'') = 1 begin
			alter table [' + @name + '] drop column [ContextOperationKind]
		end
		
		if dbm.db_model_exists_table_column(''dbo'', ''' + @name + ''', ''UsedtableModfificationDate'') = 1 begin
			alter table [' + @name + '] drop column [UsedtableModfificationDate]
		end
		use ' + @db
-- 	print @sql
	exec (@sql)
end

close TriggerAdd
deallocate TriggerAdd

go
---------------------------------------------------------------------------------------
-- STEP2: DROP TABLE
---------------------------------------------------------------------------------------

declare @sql varchar(8000)
declare @db varchar(256)
declare @postfix varchar(30)
set @postfix = (
	select	ConfigValue
	from	#scriptConfiguration
	where	Config_idn = 'DbNamePostfix'
	)

set @db = db_name()

set @sql = '
	use ' + @db + @postfix + '

	if (dbm.db_model_exists_table_column(''dbo'', ''OrderContextLog'', NULL) = 1) begin 
		drop table OrderContextLog
	end
	if (dbm.db_model_exists_table_column(''dbo'', ''OrderContext'', NULL) = 1) begin 
		drop table OrderContext
	end
	if (dbm.db_model_exists_table_column(''dbo'', ''OrderContextConfig'', NULL) = 1) begin 
		drop table OrderContextConfig
	end
	if (dbm.db_model_exists_table_column(''dbo'', ''ContextComment'', NULL) = 1) begin 
		drop table ContextComment
	end

	use ' + @db

exec(@sql)


go
---------------------------------------------------------------------------------------
-- STEP4: DROP TRIGGERS
---------------------------------------------------------------------------------------
declare @sql varchar(8000)
declare @name varchar(256)

declare TriggerAdd cursor for
	select	distinct
		o1.[oname]
	from	dbm.db_model_get_tables_columns o1
	where 	o1.oname not in (
		select	distinct
			oname
		from	dbm.db_model_get_tables_columns
		where	alterable = 'N' and cname <> 'TableRowVersion'
	)
for read only

open TriggerAdd

while 1=1 begin
	fetch next from TriggerAdd into @name

	if @@fetch_status <> 0 begin
		break
	end
	
	set @sql = '
		if object_id(''tr_' + @name + 'OrderContext'') is not null begin
			drop trigger tr_' + @name + 'OrderContext
		end
		'
-- 	print @sql
	exec (@sql)
end

close TriggerAdd
deallocate TriggerAdd

go

---------------------------------------------------------------------------------------
-- STEP5: PROCEDURES
---------------------------------------------------------------------------------------
declare @db varchar(256)
declare @dbp varchar(256)
declare @postfix varchar(30)

set @postfix = (
	select	ConfigValue
	from	#scriptConfiguration
	where	Config_idn = 'DbNamePostfix'
	)
set @db = db_name()
set @dbp = @db + @postfix

exec('use ' + @dbp)
go

if object_id('showContext') is not null begin
	drop procedure showContext
end
go

if object_id('deleteContext') is not null begin
	drop procedure deleteContext
end
go

if object_id('changeContext') is not null begin
	drop procedure changeContext
end

go
declare @db varchar(256)
set @db = db_name()

exec('use ' + @db)
go

