EXEC sp_changedbowner 'sa'
EXEC sys.sp_cdc_enable_db
GO

DECLARE enableCDC CURSOR FAST_FORWARD FOR
	SELECT
		s.name AS sname,
		T.name AS tname
	FROM	sys.tables AS T
		INNER JOIN sys.schemas AS S ON T.schema_id = S.schema_id
	WHERE	S.name = 'dbo'
	
DECLARE @sTableName sysname
DECLARE @sSchemaName sysname
DECLARE @sCaptureInstance sysname
DECLARE @sTrackingSet varchar(10) = 'set1'

OPEN enableCDC

WHILE 1 = 1
BEGIN
	FETCH NEXT FROM enableCDC INTO @sSchemaName, @sTableName
	IF @@FETCH_STATUS <> 0
	BEGIN
		BREAK
	END
	
	SET @sCaptureInstance = @sSchemaName + '_' + @sTableName + '_' + @sTrackingSet
	
	BEGIN TRY
		EXEC sys.sp_cdc_enable_table
			@source_schema = @sSchemaName,
			@source_name = @sTableName,
			@capture_instance = @sCaptureInstance,
			@role_name = NULL
	END TRY
	BEGIN CATCH
		PRINT ERROR_MESSAGE()
		IF @@TRANCOUNT > 0
		BEGIN
			PRINT 'INSIDE: Transaction in progress. Will be rollbacked'
			ROLLBACK TRANSACTION
		END
	END CATCH

    
END

CLOSE enableCDC
DEALLOCATE enableCDC
GO