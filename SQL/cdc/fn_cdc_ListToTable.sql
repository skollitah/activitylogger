IF OBJECT_ID('dbo.fn_cdc_ListToTable', 'TF') IS NOT NULL
BEGIN
	DROP FUNCTION dbo.fn_cdc_ListToTable
END
GO

CREATE FUNCTION dbo.fn_cdc_ListToTable  (@sList nvarchar(max), @sSepChar varchar(1))
RETURNS @retMyTable TABLE (sList nvarchar(max))
AS
BEGIN
	DECLARE @nCharCount int, @nCnt int, @sSingleString nvarchar(max)
	SELECT 
			@nCharCount = LEN(@sList)
		,	@sSingleString = ''

	SELECT 
			@nCharCount = LEN(@sList)

	WHILE  CHARINDEX(@sSepChar, @sList)  > 0
	BEGIN
		SET @sSingleString = SUBSTRING(@sList, 1, CHARINDEX(@sSepChar, @sList) - 1)
		SET @sList = SUBSTRING(@sList, CHARINDEX(@sSepChar, @sList) + 1, @nCharCount)
		IF LTRIM(RTRIM(@sSingleString)) <> ''
		BEGIN
			INSERT @retMyTable
			SELECT LTRIM(RTRIM(@sSingleString))
		END
	END 

	IF LTRIM(RTRIM(@sList)) <> ''
	BEGIN
		INSERT @retMyTable
		SELECT LTRIM(RTRIM(@sList))
	END
	
	RETURN 
END 
