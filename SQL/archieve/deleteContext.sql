if object_id('deleteContext') is not null begin
	drop procedure deleteContext
end
go

create procedure deleteContext (
	@context_idn varchar(100)
	,@sequence int = null
	,@comment_idn [IDL]= null
)
as begin

-- deleteContext 'default'

	set nocount on

	if not exists (select 1 from OrderContext where OrderContext_idn = @context_idn) begin
		raiserror('Invalid Context', 16 , 1)
		return
	end
	
	if @comment_idn is not null and not exists (select 1 from ContextComment where Comment_idn = @comment_idn) begin
		raiserror('Comment with given name not exists', 16 , 1)
		return
	end

	declare deleteContext cursor for
		select	distinct
			ocl.UsedTableName
			,ocl.ExternalContext_id
		from	OrderContextLog ocl
			join OrderContext oc on ocl.OrderContext_id = oc.OrderContext_id
			left join ContextComment cc on ocl.ContextComment_id = cc.ContextComment_id
		where	oc.OrderContext_idn = @context_idn
			and isnull(@sequence, ocl.ContextSequence) = ocl.ContextSequence
			and (ocl.ContextComment_id is null or isnull(@comment_idn, cc.Comment_idn) = cc.Comment_idn)
				
	open deleteContext

	declare @name varchar(256)
	declare @row bigint
	declare @sql varchar(2000)
	
	while 1=1 begin
		fetch next from deleteContext into @name, @row
		if @@fetch_status <> 0 begin
			break
		end
		set @sql = 'delete t from [' + @name + 'History] t where t.ExternalContext_id = ' + convert(varchar(20), @row)
		print @sql
		exec(@sql)

		set @sql = 'delete ocl from OrderContextLog ocl join OrderCOntext oc on oc.OrderContext_id = ocl.OrderContext_id where OrderContext_idn = ''' + @context_idn + ''' and ocl.ExternalContext_id = ' + convert(varchar(20), @row)
		print @sql
		exec(@sql)
		
	end

	if not exists (
		select	1
		from	OrderContextLog ocl
			join OrderContext oc on ocl.OrderContext_id = oc.OrderContext_id
				and oc.OrderContext_idn = @context_idn
	) begin
		set @sql = 'update OrderContext set LastContextSequence = 0 where OrderContext_idn = ''' + @context_idn + ''''
		print @sql
		exec (@sql)
	end
	
	close deleteContext
	deallocate deleteContext
end
