DECLARE @sTrackingSet varchar(10) = 'set1'

DECLARE disableCDC CURSOR FAST_FORWARD FOR
	SELECT
		s.name AS sname,
		T.name AS tname,
		CT.capture_instance
	FROM	cdc.change_tables AS CT
		INNER JOIN sys.tables AS T ON CT.source_object_id = T.object_id
		INNER JOIN sys.schemas AS S ON T.schema_id = S.schema_id
	WHERE	RIGHT(CT.capture_instance, LEN(@sTrackingSet)) = @sTrackingSet
		OR @sTrackingSet IS NULL
	
DECLARE @sTableName sysname
DECLARE @sSchemaName sysname
DECLARE @sCaptureInstance sysname

OPEN disableCDC

WHILE 1 = 1
BEGIN
	FETCH NEXT FROM disableCDC INTO @sSchemaName, @sTableName, @sCaptureInstance
	IF @@FETCH_STATUS <> 0
	BEGIN
		BREAK
	END

	EXEC sys.sp_cdc_disable_table @source_schema = @sSchemaName,
	    @source_name = @sTableName,
	    @capture_instance = @sCaptureInstance    
END

CLOSE disableCDC
DEALLOCATE disableCDC
GO

IF OBJECT_ID('cdc.change_tables', 'U') IS NOT NULL
BEGIN
	IF NOT EXISTS (
		SELECT	*
		FROM	cdc.change_tables AS CT
	)
	BEGIN
		EXEC sys.sp_cdc_disable_db
	END
END
GO
