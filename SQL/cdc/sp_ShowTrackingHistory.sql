IF OBJECT_ID('dbo.sp_ShowTrackingHistory', 'P') IS NOT NULL
BEGIN
	DROP PROCEDURE dbo.sp_ShowTrackingHistory
END
GO

-- EXEC dbo.sp_ShowTrackingHistory 'set1', '2013-09-26 13:52:58.923', '2013-09-26 13:54:39.723'
-- EXEC dbo.sp_ShowTrackingHistory 'set1', NULL, NULL, 'Customer'

CREATE PROCEDURE dbo.sp_ShowTrackingHistory (
	@sTrackingSet varchar(10),
	@dChangeFrom datetime = NULL,
	@dChangeTo datetime = NULL,
	@sTableNames nvarchar(MAX) = NULL
)
AS BEGIN
	SET NOCOUNT ON

	IF NOT EXISTS (
		SELECT	*
		FROM	cdc.change_tables AS CT
		WHERE	RIGHT(CT.capture_instance, LEN(@sTrackingSet)) = @sTrackingSet
	)
	BEGIN
		RAISERROR('There is no tables under tracking with given TrackingSet.', 10, 1)
	END
	
	DECLARE @tTableNamesList AS TABLE (
		TableName sysname
	)
	
	INSERT INTO @tTableNamesList (
		TableName
	)
		SELECT	LT.sList
		FROM	dbo.fn_cdc_ListToTable(@sTableNames, ',') AS LT
	
	IF 	EXISTS (
		SELECT	TN.TableName
		FROM	@tTableNamesList AS TN
		EXCEPT
		SELECT	T.[name]
		FROM	cdc.change_tables AS CT
			INNER JOIN sys.tables AS T ON CT.source_object_id = T.object_id
		WHERE	RIGHT(CT.capture_instance, LEN(@sTrackingSet)) = @sTrackingSet
	)
	BEGIN
		RAISERROR('Some tables given in parameter TableNames are not tracked', 10, 1)
	END		
		
	DECLARE @dChangeFromAsTest varchar(23) = CONVERT(varchar(23), @dChangeFrom, 120)
	DECLARE @dChangeToAsTest varchar(23) = CONVERT(varchar(23), @dChangeTo, 120)
	DECLARE @sCaptureInstance sysname
	DECLARE @sSql nvarchar(MAX)
	DECLARE @nOperationSequence int
	DECLARE @sColumnList varchar(MAX)
	DECLARE @bStartLsn binary(10)
	DECLARE @bSeqval binary(10)
	DECLARE @nOperation tinyint
	DECLARE @sSourceTableName sysname
	
	IF OBJECT_ID('tempdb.dbo.#OperationMetadata', 'U') IS NOT NULL
	BEGIN
		DROP TABLE dbo.#OperationMetadata
	END
	
	CREATE TABLE dbo.#OperationMetadata (
		capture_instance sysname,
		start_lsn binary(10),
		end_lsn binary(10),
		seqval binary(10),
		operation int,
		update_mask varbinary(128)
	)
	
	DECLARE collectMetadata CURSOR FAST_FORWARD FOR
		SELECT	CT.capture_instance
		FROM	cdc.change_tables AS CT
			INNER JOIN sys.tables AS T ON CT.source_object_id = T.object_id
		WHERE	RIGHT(CT.capture_instance, LEN(@sTrackingSet)) = @sTrackingSet
			AND (T.name IN (
				SELECT	TN.TableName
				FROM	@tTableNamesList AS TN
			) OR @sTableNames IS NULL)

	OPEN collectMetadata

	WHILE 1 = 1
	BEGIN
		FETCH NEXT FROM collectMetadata INTO @sCaptureInstance
		
		IF @@FETCH_STATUS <> 0
		BEGIN
			BREAK
		END
		
		
		SET @sSql = '
			INSERT INTO dbo.#OperationMetadata (
				capture_instance,
				start_lsn,
				end_lsn,
				seqval,
				operation,
				update_mask
			)
				SELECT
					''' + @sCaptureInstance + ''',
					CI.[__$start_lsn],
					CI.[__$end_lsn],
					CI.[__$seqval],
					CI.[__$operation],
					CI.[__$update_mask]
				FROM	cdc.[' + @sCaptureInstance + '_CT] AS CI
				WHERE	(sys.fn_cdc_map_lsn_to_time(CI.[__$start_lsn]) >= @dChangeFrom OR @dChangeFrom IS NULL)
					AND (sys.fn_cdc_map_lsn_to_time(CI.[__$start_lsn]) <= @dChangeTo OR @dChangeTo IS NULL)
					AND CI.[__$operation] <> 3
			'
 
		EXEC sys.sp_executesql @sSql, N'@dChangeFrom datetime, @dChangeTo datetime', @dChangeFrom, @dChangeTo
 
	END

	CLOSE collectMetadata
	DEALLOCATE collectMetadata
	
	IF OBJECT_ID('tempdb.dbo.#AffectedColumns', 'U') IS NOT NULL
	BEGIN
		DROP TABLE dbo.#AffectedColumns
	END
	
	SELECT
		ROW_NUMBER() OVER (PARTITION BY CT.capture_instance, OM.start_lsn, OM.seqval, OM.operation ORDER BY CC.column_ordinal) AS column_id,
		ROW_NUMBER() OVER (PARTITION BY CT.capture_instance, OM.start_lsn, OM.seqval, OM.operation ORDER BY CC.column_ordinal desc) AS column_id_desc,
		CT.capture_instance,
		CC.column_name,
		OM.start_lsn,
		OM.seqval,
		OM.operation,
		T.name AS SourceTableName
	INTO	#AffectedColumns
	FROM	cdc.change_tables AS CT
		INNER JOIN cdc.captured_columns AS CC ON CT.object_id = CC.object_id
		INNER JOIN sys.tables AS T ON CT.source_object_id = T.object_id
		INNER JOIN #OperationMetadata AS OM ON CT.capture_instance = OM.capture_instance
	WHERE	sys.fn_cdc_has_column_changed(CT.capture_instance, CC.column_name, OM.update_mask) = 1
		OR EXISTS (
			SELECT	*
			FROM	cdc.index_columns AS IC
			WHERE	IC.object_id = CT.object_id
				AND IC.column_id = CC.column_id
			)
	
	CREATE CLUSTERED INDEX AffectedColumns_capture_instance_start_lsn_operation_seqval_column_id_IDX
		ON #AffectedColumns (capture_instance, start_lsn, operation, seqval, column_id)
	
	IF OBJECT_ID('tempdb.dbo.#OperationMetadataWithColumnList', 'U') IS NOT NULL
	BEGIN
		DROP TABLE dbo.#OperationMetadataWithColumnList
	END
	
	;WITH TableColumns AS (
		SELECT
			AC.column_id,
			AC.column_id_desc,
			AC.capture_instance,
			CONVERT(varchar(MAX), 'CI.[' + AC.column_name + ']') AS column_list,
			AC.start_lsn,
			AC.seqval,
			AC.operation,
			AC.SourceTableName
		FROM	#AffectedColumns AS AC
		WHERE	AC.column_id = 1
		UNION	ALL
		SELECT
			nextCol.column_id,
			nextCol.column_id_desc,
			prevCol.capture_instance,
			CONVERT(varchar(MAX), prevCol.column_list + ',CI.[' + nextCol.column_name + ']'),
			prevCol.start_lsn,
			prevCol.seqval,
			prevCol.operation,
			prevCol.SourceTableName
		FROM	TableColumns AS prevCol
			INNER JOIN #AffectedColumns AS nextCol ON prevCol.column_id + 1 = nextCol.column_id
				AND prevCol.capture_instance = nextCol.capture_instance
				AND prevCol.start_lsn = nextCol.start_lsn
				AND prevCol.operation = nextCol.operation	
				AND prevCol.seqval = nextCol.seqval
	)	
		SELECT
			ROW_NUMBER() OVER (ORDER BY sys.fn_cdc_map_lsn_to_time(TC.start_lsn), TC.operation) AS operation_sequence,
			TC.capture_instance,
			TC.column_list,
			TC.start_lsn,
			TC.seqval,
			TC.operation,
			TC.SourceTableName
		INTO	#OperationMetadataWithColumnList
		FROM	TableColumns AS TC
		WHERE	TC.column_id_desc = 1
		OPTION (MAXRECURSION 300)
	
	
	DECLARE printHistory CURSOR FAST_FORWARD FOR
		SELECT
			OM.operation_sequence,
			OM.capture_instance,
			OM.column_list,
			OM.start_lsn,
			OM.seqval,
			OM.operation,
			OM.SourceTableName
		FROM	#OperationMetadataWithColumnList AS OM
		ORDER BY
			OM.operation_sequence
	
	OPEN printHistory
		
	WHILE 1 = 1
	BEGIN
		FETCH NEXT FROM printHistory INTO @nOperationSequence, @sCaptureInstance, @sColumnList, @bStartLsn, @bSeqval, @nOperation, @sSourceTableName
		
		IF @@FETCH_STATUS <> 0
		BEGIN
			BREAK
		END
		
		SET @sSql = '
			SELECT
				@nOperationSequence AS [Seq],
				sys.fn_cdc_map_lsn_to_time(CI.[__$start_lsn]) AS [Time],
				@sSourceTableName AS [Table],
				CASE
					WHEN CI.[__$operation] = 1 THEN ''DELETE''
					WHEN CI.[__$operation] = 2 THEN ''INSERT''
					WHEN CI.[__$operation] IN (3, 4) THEN ''UPDATE''
				END AS [Type],
				' + @sColumnList + '
			FROM	cdc.[' + @sCaptureInstance + '_CT] AS CI
			WHERE	CI.[__$start_lsn] = @bStartLsn
				AND CI.[__$operation] = @nOperation
				AND CI.[__$seqval] = @bSeqval
			'
		PRINT 	@sSql
		EXEC sys.sp_executesql @sSql, N'@nOperationSequence int, @sSourceTableName sysname, @bStartLsn binary(10), @nOperation tinyint, @bSeqval binary(10)',
			@nOperationSequence, @sSourceTableName, @bStartLsn, @nOperation, @bSeqval
 
	END

	CLOSE printHistory
	DEALLOCATE printHistory
END

