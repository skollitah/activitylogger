EXEC dbm.db_model_utl_createProc 'db_model_generate_tables_columns_forhistory'
GO

ALTER PROCEDURE dbm.db_model_generate_tables_columns_forhistory
(
	@schname sysname,
	@source bit, -- 0 - database, 1 - saved objects
	@output smallint, -- 0 - table, 1 - console, 2 - execute 
	@oname sysname = NULL,
	@cname sysname = NULL
)
AS BEGIN

/*
EXEC dbm.db_model_save_tables_columns
DROP TABLE dbm.db_model_saved_tables_columns

EXEC dbm.db_model_generate_tables_columns_forhistory 'dbo', 1, 0, NULL, NULL
EXEC dbm.db_model_generate_tables_columns_forhistory 'dbo', 0, 0, 'ForeignIdentifier', 'ForeignIdentifier_id'
EXEC dbm.db_model_generate_tables_columns_forhistory 'dbo', 0, 1, 'ForeignIdentifier', NULL

SELECT * FROM dbm.db_model_get_tables_columns
*/
	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF

	IF OBJECT_ID('tempdb..#result') IS NOT NULL
	BEGIN
		DROP TABLE #result
	END
	
	CREATE TABLE #result
	(
		[row] numeric(10) identity(1,1),
		[batchend] bit,
		[code] varchar(MAX),
	)

	IF OBJECT_ID('tempdb..#db_objects') IS NOT NULL
	BEGIN
		DROP TABLE #db_objects
	END

	CREATE TABLE #db_objects (
		[id] numeric(10) identity(1,1),
		[schname] sysname NULL,
		[oname] sysname NULL,
		[cname] sysname NULL,
		[tname] varchar(300) NULL,
		[nullable] varchar(10) NULL,
		[collation] sysname NULL,
		[colorder] smallint NULL,
		[alterable] varchar(2) NULL,
		[autoincrement] varchar(20) NULL
	)

	IF @source = 0
	BEGIN
		INSERT INTO #db_objects (
			[schname],
			[oname],
			[cname],
			[tname],
			[nullable],
			[collation],
			[colorder],
			[alterable],
			[autoincrement]
		)
			SELECT	DISTINCT
				[schname],
				[oname],
				[cname],
				CASE	
					WHEN G.[stname] IS NULL THEN G.[utname]
					ELSE '[' + G.[utname] + ']'
				END,
				CASE
					WHEN G.[nullable] = 'Y' THEN 'NULL'
					WHEN G.[nullable] = 'N' THEN 'NOT NULL'
				END,
				CASE
					WHEN G.[collation] IS NULL OR G.[stname] IS NOT NULL THEN NULL
					ELSE G.[collation]
				END,
				[colorder],
				[alterable],
				NULL
			FROM	dbm.db_model_get_tables_columns AS G
			WHERE	
				(
				G.oname = @oname
					AND G.cname = @cname
				OR	G.oname = @oname
					AND @cname IS NULL
				OR	@oname IS NULL
					AND @cname IS NULL
				)
				AND G.schname = @schname
			ORDER BY
				G.[oname],
				G.[colorder]
	END
	ELSE IF @source = 1 AND dbm.db_model_exists_table_column('dbm', 'db_model_saved_tables_columns', NULL) = 1
	BEGIN
		INSERT INTO #db_objects (
			[schname],
			[oname],
			[cname],
			[tname],
			[nullable],
			[collation],
			[colorder],
			[alterable],
			[autoincrement]
		)
			SELECT	DISTINCT
				[schname],
				[oname],
				[cname],
				CASE	
					WHEN G.[stname] IS NULL THEN G.[utname]
					ELSE '[' + G.[utname] + ']'
				END,
				CASE
					WHEN G.[nullable] = 'Y' THEN 'NULL'
					WHEN G.[nullable] = 'N' THEN 'NOT NULL'
				END,
				CASE
					WHEN G.[collation] IS NULL OR G.[stname] IS NOT NULL THEN NULL
					ELSE G.[collation]
				END,
				[colorder],
				[alterable],
				NULL
			FROM	dbm.db_model_saved_tables_columns AS G
			WHERE	
				(
				G.oname = @oname
					AND G.cname = @cname
				OR	G.oname = @oname
					AND @cname IS NULL
				OR	@oname IS NULL
					AND @cname IS NULL
				)
				AND G.schname = @schname
			ORDER BY
				G.[oname],
				G.[colorder]
	END
	
	DECLARE @ischname sysname
	DECLARE @ioname sysname
	DECLARE @icname sysname
	DECLARE @itname varchar(300)
	DECLARE @inullable varchar(10)
	DECLARE @icollation sysname
	DECLARE @ialterable varchar(2)
	DECLARE @iautoincrement varchar(20)
	DECLARE @tmpResult varchar(8000) = ''
	DECLARE @nl varchar(2) = CHAR(13) + CHAR(10)

	DECLARE generateObjects CURSOR FAST_FORWARD FOR
		SELECT
			DISTINCT
			O.schname,
			O.[oname]
		FROM	#db_objects AS O
		ORDER BY
			O.[oname]
	
	OPEN generateObjects
	
	FETCH NEXT FROM generateObjects INTO @ischname, @ioname
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @tmpResult = ''
		SET @tmpResult = @tmpResult +  '-- ' + UPPER(@ioname) + @nl
		SET @tmpResult = @tmpResult +  'IF dbm.db_model_exists_table_column(''' + @ischname + ''', ''' + @ioname + ''', NULL) = 0' + @nl
		SET @tmpResult = @tmpResult +  'BEGIN' + @nl
		SET @tmpResult = @tmpResult +  '	CREATE TABLE [' + @ischname + '].[' + @ioname + '] ([db_model_fake_column] smallint)' + @nl
		SET @tmpResult = @tmpResult +  'END' + @nl
		INSERT INTO #result(batchend, code) VALUES(0, @tmpResult)

		DECLARE generateColumns CURSOR FAST_FORWARD FOR
			SELECT
				O.[cname],
				O.[tname],
				O.[nullable],
				O.[collation],
				O.[alterable],
				O.[autoincrement]				
			FROM	#db_objects AS O
			WHERE	O.oname = @ioname
				AND O.schname = @ischname
			ORDER BY
				O.[colorder]
		
		OPEN generateColumns
		
		FETCH NEXT FROM generateColumns INTO @icname, @itname, @inullable, @icollation, @ialterable, @iautoincrement
		WHILE @@FETCH_STATUS = 0
		BEGIN
			SET @tmpResult = ''
			SET @tmpResult = @tmpResult + 'IF dbm.db_model_exists_table_column(''' + @ischname + ''', ''' + @ioname + ''', ''' + @icname + ''') = 0' + @nl
			SET @tmpResult = @tmpResult + 'BEGIN' + @nl
			SET @tmpResult = @tmpResult + '	ALTER TABLE [' + @ischname + '].[' + @ioname + '] ADD [' + @icname + '] ' + @itname + ISNULL(' COLLATE ' + @icollation + ' ', ' ') +  @inullable +  ISNULL(' ' + @iautoincrement, '') + @nl
			SET @tmpResult = @tmpResult + 'END'	+ @nl
			IF @ialterable = 'Y'
			BEGIN
				SET @tmpResult = @tmpResult + 'ALTER TABLE [' + @ischname + '].[' + @ioname + '] ALTER COLUMN [' + @icname + '] ' + @itname + ISNULL(' COLLATE ' + @icollation + ' ', ' ') +  @inullable + @nl
			END
			ELSE
			BEGIN
				SET @tmpResult = @tmpResult + 'PRINT ''Column [' + @ischname + '].[' + @ioname + '].[' + @icname + '] can''''t be altered''' + @nl
			END
			INSERT INTO #result(batchend, code) VALUES(0, @tmpResult)
			
			FETCH NEXT FROM generateColumns INTO @icname, @itname, @inullable, @icollation, @ialterable, @iautoincrement
		END

		CLOSE generateColumns
		DEALLOCATE generateColumns
		
		
		SET @tmpResult = ''
		SET @tmpResult = @tmpResult +  'IF dbm.db_model_exists_table_column(''' + @ischname + ''', ''' + @ioname + ''', ''db_model_fake_column'') = 1' + @nl
		SET @tmpResult = @tmpResult +  'BEGIN' + @nl
		SET @tmpResult = @tmpResult +  '	ALTER TABLE [' + @ischname + '].[' + @ioname + '] DROP COLUMN [db_model_fake_column]' + @nl
		SET @tmpResult = @tmpResult +  'END' + @nl
		INSERT INTO #result(batchend, code) VALUES(1, @tmpResult)
		
		FETCH NEXT FROM generateObjects INTO @ischname, @ioname
	END
	
	IF @output = 0
	BEGIN
		SELECT	*
		FROM	#result
	END
	ELSE IF @output = 1
	BEGIN
		EXEC dbm.db_model_print_output '#result'
	END
	ELSE IF @output = 2
	BEGIN
		EXEC dbm.db_model_execute_output '#result'
	END
		
	CLOSE generateObjects
	DEALLOCATE generateObjects		
END

