---------------------------------------------------------------------------------------
-- STEP0: SCRIPT CONFIGURATION 
---------------------------------------------------------------------------------------
if exists (SELECT 1 FROM tempdb..sysobjects WHERE [ID] = OBJECT_ID('tempdb..#scriptConfiguration') AND type = 'U') begin
	drop table #scriptConfiguration
end
	
create table #scriptConfiguration (
	Config_idn varchar(30)
	,Config_desc varchar(500)
	,ConfigValue varchar(2000)
)

insert into #scriptConfiguration(Config_idn, Config_desc, ConfigValue)
	select	'TableNamePostfix', '', ''
	union
	select	'DbNamePostfix', '', '_history'

select	Config_idn
	,Config_desc
	,ConfigValue
from	#scriptConfiguration
go

---------------------------------------------------------------------------------------
-- STEP1: CREATE LOG DATABASE
---------------------------------------------------------------------------------------
-- 
-- declare @sql varchar(8000)
-- declare @db varchar(256)
-- declare @postfix varchar(30)
-- set @postfix = (
-- 	select	ConfigValue
-- 	from	#scriptConfiguration
-- 	where	Config_idn = 'DbNamePostfix'
-- 	)
-- set @db = db_name()
-- 
-- if not exists (
-- 	select	1
-- 	from	master..sysdatabases
-- 	where	[name] = @db + @postfix
-- ) begin
-- 	exec('create database ' + @db + @postfix)
-- end
-- 
-- go

---------------------------------------------------------------------------------------
-- STEP2: CREATE TABLES
---------------------------------------------------------------------------------------

declare @sql varchar(8000)
declare @db varchar(256)
declare @postfix varchar(30)
set @postfix = (
	select	ConfigValue
	from	#scriptConfiguration
	where	Config_idn = 'DbNamePostfix'
	)

set @db = db_name()


exec('
	use ' + @db + @postfix + '
	if (dbm.db_model_exists_table_column(''dbo'', ''ContextComment'', NULL) = 0) begin 
		create table dbo.ContextComment (
			ContextComment_id [ID] identity(1, 1) not null primary key clustered
			,Comment_idn [IDL] not null
			,Comment varchar(2000)
		)
	end
	
	if (dbm.db_model_exists_table_column(''dbo'', ''OrderContextConfig'', NULL) = 0) begin
		create table dbo.OrderContextConfig (
			ContextCaptureActive smallint
		)
	end
	
	
	if (dbm.db_model_exists_table_column(''dbo'', ''OrderContext'', NULL) = 0) begin 
		create table dbo.OrderContext (
			OrderContext_id [ID] identity(1, 1) not null primary key clustered
			,OrderContext_idn [IDL] not null
			,OrderContextActive smallint not null default 0
			,LastContextSequence int not null default 0
			,ContextComment_id [ID] foreign key references ContextComment(ContextComment_id)
		
		)
	end
	
	if (dbm.db_model_exists_table_column(''dbo'', ''OrderContextLog'', NULL) = 0) begin 
		create table dbo.OrderContextLog (
			OrderContextLog_id [ID] NOT NULL IDENTITY(1, 1) PRIMARY KEY NONCLUSTERED
			,OrderContext_id [ID] not null foreign key references OrderContext(OrderContext_id)
			,UsedTableName sysname not null
			,UsedtableModfificationDate datetime not null
			,UsedTableModificationType varchar(15) not null
			,ContextSequence int not null
			,ContextComment_id [ID] foreign key references ContextComment(ContextComment_id)
		)

		CREATE CLUSTERED INDEX 	OrderContextLog_UsedtableModfificationDate_UsedTableName_OrderContext_id_IDX ON dbo.OrderContextLog(UsedtableModfificationDate, UsedTableName, OrderContext_id)
	end
	use ' + @db)
go
---------------------------------------------------------------------------------------
-- STEP3: INITIALIZE TABLES
---------------------------------------------------------------------------------------
declare @sql varchar(8000)
declare @db varchar(256)
declare @dbp varchar(256)
declare @postfix varchar(30)
set @postfix = (
	select	ConfigValue
	from	#scriptConfiguration
	where	Config_idn = 'DbNamePostfix'
	)

set @db = db_name()
set @dbp = @db + @postfix
	
set @sql = '
	use ' + @dbp + '
	insert into dbo.OrderContextConfig (ContextCaptureActive)  values (1)
	insert into dbo.OrderContext (OrderContext_idn,OrderContextActive) values (''default'', 1)
	insert into dbo.OrderContext(OrderContext_idn, OrderContextActive) values (''user_login'', 0)
	insert into dbo.OrderContext(OrderContext_idn, OrderContextActive) values (''user_logout'', 0)
	insert into dbo.OrderContext(OrderContext_idn, OrderContextActive) values (''pre_process'', 0)
	insert into dbo.OrderContext(OrderContext_idn, OrderContextActive) values (''process'', 0)
	insert into dbo.OrderContext(OrderContext_idn, OrderContextActive) values (''dividend_calc'', 0)
	
	insert into dbo.OrderContext (OrderContext_idn, OrderContextActive)
		select	DocumentType_idn + ''.maker''
			,1
		from	' + @db + '..DocumentType
		union
		select	DocumentType_idn + ''.checker''
			,0
		from	' + @db + '..DocumentType
		order by
			1, 2
	
	update	dbo.OrderContext
	set	OrderContextActive = 0
	
	update	dbo.OrderContext
	set	OrderContextActive = 1
	where	OrderContext_idn = ''default''
	use ' + @db
-- print @sql
exec(@sql)

go
---------------------------------------------------------------------------------------
-- STEP3: ADD COLUMNS
---------------------------------------------------------------------------------------
declare @sql varchar(8000)
declare @name varchar(256)
declare @db varchar(256)
declare @dbp varchar(256)
declare @postfix varchar(30)
declare @tpostfix varchar(15)

set @postfix = (
	select	ConfigValue
	from	#scriptConfiguration
	where	Config_idn = 'DbNamePostfix'
	)
set @tpostfix = (
	select	ConfigValue
	from	#scriptConfiguration
	where	Config_idn = 'TableNamePostfix'
	)

set @db = db_name()
set @dbp = @db + @postfix

declare TriggerAdd cursor for
	select	distinct
		o1.[oname]
	from	dbm.db_model_get_tables_columns o1
	where 	o1.oname not in (
		select	distinct
			oname
		from	dbm.db_model_get_tables_columns
		where	alterable = 'N' and cname <> 'TableRowVersion'
	)
	AND o1.schname = 'dbo'
for read only

open TriggerAdd


while 1=1 begin
	fetch next from TriggerAdd into @name

	if @@fetch_status <> 0 begin
		break
	end

	set @name = @name + @tpostfix

	set @sql = '
		use ' + @dbp + '
		if dbm.db_model_exists_table_column(''dbo'', ''' + @name + ''', ''ContextOperationKind'') = 0 begin
			alter table dbo.[' + @name + '] add [ContextOperationKind] smallint
		end

		if dbm.db_model_exists_table_column(''dbo'', ''' + @name + ''', ''UsedtableModfificationDate'') = 0 begin
			alter table dbo.[' + @name + '] add UsedtableModfificationDate datetime
		end
		use ' + @db

	exec(@sql)

end

-- producthistory

close TriggerAdd
deallocate TriggerAdd

go
---------------------------------------------------------------------------------------
-- STEP4: ADD TRIGGERS
---------------------------------------------------------------------------------------
declare @sql varchar(8000)
declare @name varchar(256)
declare @db varchar(256)
declare @dbp varchar(256)
declare @postfix varchar(30)
declare @tpostfix varchar(15)

set @postfix = (
	select	ConfigValue
	from	#scriptConfiguration
	where	Config_idn = 'DbNamePostfix'
	)
set @tpostfix = (
	select	ConfigValue
	from	#scriptConfiguration
	where	Config_idn = 'TableNamePostfix'
	)

set @db = db_name()
set @dbp = @db + @postfix

declare TriggerAdd cursor for
	select	distinct
		o1.[oname]
	from	dbm.db_model_get_tables_columns o1
	where 	o1.oname not in (
		select	distinct
			oname
		from	dbm.db_model_get_tables_columns
		where	alterable = 'N' and cname <> 'TableRowVersion'
	)
	AND o1.schname = 'dbo'
for read only

open TriggerAdd

while 1=1 begin
	fetch next from TriggerAdd into @name

	if @@fetch_status <> 0 begin
		break
	end
	
	set @sql = '
		if object_id(''tr_' + @name + 'OrderContext'') is not null begin
			drop trigger tr_' + @name + 'OrderContext
		end
		'

	exec(@sql)

	set @sql = '
		create trigger tr_' + @name + 'OrderContext  on [' + @name + '] after update, insert, delete
		as begin
			set nocount on
			declare @ExtenralContext_id bigint
			if (select ContextCaptureActive from [' + @dbp + ']..OrderContextConfig) = 1 begin
				declare @modificationDate datetime
				set @modificationDate = GETDATE()
				

				if (select count(*) from inserted) > 0  and (select count(*) from deleted) > 0 begin 
					insert into [' + @dbp + ']..OrderContextLog (OrderContext_id, UsedTableName, UsedtableModfificationDate, UsedTableModificationType, ContextSequence, ContextComment_id)
						select	(select OrderContext_id from [' + @dbp + ']..OrderContext where OrderContextActive = 1)
							,''' + @name + '''
							,@modificationDate
							,''update''
							,(select LastContextSequence from [' + @dbp + ']..OrderContext where OrderContextActive = 1)
							,(select ContextComment_id from [' + @dbp + ']..OrderContext where OrderContextActive = 1)
						from	inserted
					insert into [' + @dbp + ']..[' +@name + @tpostfix + ']
						select	d.*, 0, @modificationDate from deleted d
					insert into [' + @dbp + ']..[' +@name + @tpostfix + ']
						select	i.*, 1, @modificationDate from inserted i
	
				end
				else begin
					insert into [' + @dbp + ']..OrderContextLog (OrderContext_id, UsedTableName, UsedtableModfificationDate, UsedTableModificationType, ContextSequence, ContextComment_id)
						select	(select OrderContext_id from [' + @dbp + ']..OrderContext where OrderContextActive = 1)
							,''' + @name + '''
							,@modificationDate
							,''insert''
							,(select LastContextSequence from [' + @dbp + ']..OrderContext where OrderContextActive = 1)
							,(select ContextComment_id from [' + @dbp + ']..OrderContext where OrderContextActive = 1)
						from	inserted
						union
						select	(select OrderContext_id from [' + @dbp + ']..OrderContext where OrderContextActive = 1)
							,''' + @name + '''
							,@modificationDate
							,''delete''
							,(select LastContextSequence from [' + @dbp + ']..OrderContext where OrderContextActive = 1)
							,(select ContextComment_id from [' + @dbp + ']..OrderContext where OrderContextActive = 1)
						from	deleted
					insert into [' + @dbp + ']..[' +@name + @tpostfix + ']
						select	d.*, 0, @modificationDate from deleted d
					insert into [' + @dbp + ']..[' +@name + @tpostfix + ']
						select	i.*, 1, @modificationDate from inserted i
				end
			end
		end'
	--PRINT @sql
	exec(@sql)
end

close TriggerAdd
deallocate TriggerAdd

go
SELECT * FROM sys.objects WHERE type = 'u' AND schema_id <> 1