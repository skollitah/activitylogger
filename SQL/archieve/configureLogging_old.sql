---------------------------------------------------------------------------------------
-- STEP0: SCRIPT CONFIGURATION 
---------------------------------------------------------------------------------------
if exists (SELECT 1 FROM tempdb..sysobjects WHERE [ID] = OBJECT_ID('tempdb..#scriptConfiguration') AND type = 'U') begin
	drop table #scriptConfiguration
end
	
create table #scriptConfiguration (
	Config_idn varchar(30)
	,Config_desc varchar(500)
	,ConfigValue varchar(2000)
)

insert into #scriptConfiguration(Config_idn, Config_desc, ConfigValue)
	select	'TableNamePostfix', '', 'History'
	union
	select	'DbNamePostfix', '', '_history'

select	Config_idn
	,Config_desc
	,ConfigValue
from	#scriptConfiguration
go

---------------------------------------------------------------------------------------
-- STEP1: CREATE LOG DATABASE
---------------------------------------------------------------------------------------
-- 
-- declare @sql varchar(8000)
-- declare @db varchar(256)
-- declare @postfix varchar(30)
-- set @postfix = (
-- 	select	ConfigValue
-- 	from	#scriptConfiguration
-- 	where	Config_idn = 'DbNamePostfix'
-- 	)
-- set @db = db_name()
-- 
-- if not exists (
-- 	select	1
-- 	from	master..sysdatabases
-- 	where	[name] = @db + @postfix
-- ) begin
-- 	exec('create database ' + @db + @postfix)
-- end
-- 
-- go

---------------------------------------------------------------------------------------
-- STEP2: CREATE TABLES
---------------------------------------------------------------------------------------

declare @sql varchar(8000)
declare @db varchar(256)
declare @postfix varchar(30)
set @postfix = (
	select	ConfigValue
	from	#scriptConfiguration
	where	Config_idn = 'DbNamePostfix'
	)

set @db = db_name()


exec('
	use ' + @db + @postfix + '
	if (dbo.utl_tableExists(''ContextComment'') = 0) begin 
		create table ContextComment (
			ContextComment_id [ID] identity(1, 1) not null primary key clustered
			,Comment_idn [IDL] not null
			,Comment varchar(2000)
		)
	end
	
	if (dbo.utl_tableExists(''OrderContextConfig'') = 0) begin
		create table OrderContextConfig (
			ContextCaptureActive smallint
			,LastExternalContext bigint
		)
	end
	
	
	if (dbo.utl_tableExists(''OrderContext'') = 0) begin 
		create table OrderContext (
			OrderContext_id [ID] identity(1, 1) not null primary key clustered
			,OrderContext_idn [IDL] not null
			,OrderContextActive smallint not null default 0
			,LastContextSequence int not null default 0
			,ContextComment_id [ID] foreign key references ContextComment(ContextComment_id)
		
		)
	end
	
	if (dbo.utl_tableExists(''OrderContextLog'') = 0) begin 
		create table OrderContextLog (
			,OrderContext_id [ID] not null foreign key references OrderContext(OrderContext_id)
			,UsedTableName sysname not null
			,UsedtableModfificationDate datetime not null
			,UsedTableModificationType varchar(15) not null
			,ExternalContext_id bigint no null
			,ContextSequence int not null
			,ContextComment_id [ID] foreign key references ContextComment(ContextComment_id)
		)
		
		
ALTER TABLE OrderContextLog ADD CONSTRAINT OrderContextLog_PK PRIMARY KEY CLUSTERED  (UsedtableModfificationDate, ExternalContext_id)
	end
	use ' + @db)
go
---------------------------------------------------------------------------------------
-- STEP3: INITIALIZE TABLES
---------------------------------------------------------------------------------------
declare @sql varchar(8000)
declare @db varchar(256)
declare @dbp varchar(256)
declare @postfix varchar(30)
set @postfix = (
	select	ConfigValue
	from	#scriptConfiguration
	where	Config_idn = 'DbNamePostfix'
	)

set @db = db_name()
set @dbp = @db + @postfix
	
set @sql = '
	use ' + @dbp + '
	insert into OrderContextConfig (ContextCaptureActive, LastExternalContext)  values (1, 1)
	insert into OrderContext (OrderContext_idn,OrderContextActive) values (''default'', 1)
	insert into OrderContext(OrderContext_idn, OrderContextActive) values (''user_login'', 0)
	insert into OrderContext(OrderContext_idn, OrderContextActive) values (''user_logout'', 0)
	insert into OrderContext(OrderContext_idn, OrderContextActive) values (''pre_process'', 0)
	insert into OrderContext(OrderContext_idn, OrderContextActive) values (''process'', 0)
	insert into OrderContext(OrderContext_idn, OrderContextActive) values (''dividend_calc'', 0)
	
	insert into OrderContext (OrderContext_idn, OrderContextActive)
		select	DocumentType_idn + ''.maker''
			,1
		from	' + @db + '..DocumentType
		union
		select	DocumentType_idn + ''.checker''
			,0
		from	' + @db + '..DocumentType
		order by
			1, 2
	
	update	OrderContext
	set	OrderContextActive = 0
	
	update	OrderContext
	set	OrderContextActive = 1
	where	OrderContext_idn = ''default''
	use ' + @db
-- print @sql
exec(@sql)

go
---------------------------------------------------------------------------------------
-- STEP3: ADD COLUMNS
---------------------------------------------------------------------------------------
declare @sql varchar(8000)
declare @name varchar(256)
declare @db varchar(256)
declare @dbp varchar(256)
declare @postfix varchar(30)
declare @tpostfix varchar(15)

set @postfix = (
	select	ConfigValue
	from	#scriptConfiguration
	where	Config_idn = 'DbNamePostfix'
	)
set @tpostfix = (
	select	ConfigValue
	from	#scriptConfiguration
	where	Config_idn = 'TableNamePostfix'
	)

set @db = db_name()
set @dbp = @db + @postfix

declare TriggerAdd cursor for
	select	distinct
		o1.[oname]
	from	dbo.db_model_get_tables_columns o1
	where 	o1.oname not in (
		select	distinct
			oname
		from	dbo.db_model_get_tables_columns
		where	alterable = 'N' and cname <> 'TableRowVersion'
	)
for read only

open TriggerAdd


while 1=1 begin
	fetch next from TriggerAdd into @name

	if @@fetch_status <> 0 begin
		break
	end

	set @name = @name + @tpostfix

	set @sql = '
		use ' + @dbp + '
		if dbo.utl_columnExists(''' + @name + ''', ''ContextOperationKind'') = 0 begin
			alter table [' + @name + '] add [ContextOperationKind] smallint
		end

		if dbo.utl_columnExists(''' + @name + ''', ''ExternalContext_id'') = 0 begin
			alter table [' + @name + '] add [ExternalContext_id] bigint
		end
		use ' + @db

	exec(@sql)

end

-- producthistory

close TriggerAdd
deallocate TriggerAdd

go
---------------------------------------------------------------------------------------
-- STEP4: ADD TRIGGERS
---------------------------------------------------------------------------------------
declare @sql varchar(8000)
declare @name varchar(256)
declare @db varchar(256)
declare @dbp varchar(256)
declare @postfix varchar(30)
declare @tpostfix varchar(15)

set @postfix = (
	select	ConfigValue
	from	#scriptConfiguration
	where	Config_idn = 'DbNamePostfix'
	)
set @tpostfix = (
	select	ConfigValue
	from	#scriptConfiguration
	where	Config_idn = 'TableNamePostfix'
	)

set @db = db_name()
set @dbp = @db + @postfix

declare TriggerAdd cursor for
	select	distinct
		o1.[oname]
	from	dbo.db_model_get_tables_columns o1
	where 	o1.oname not in (
		select	distinct
			oname
		from	dbo.db_model_get_tables_columns
		where	alterable = 'N' and cname <> 'TableRowVersion'
	)
for read only

open TriggerAdd

while 1=1 begin
	fetch next from TriggerAdd into @name

	if @@fetch_status <> 0 begin
		break
	end
	
	set @sql = '
		if object_id(''tr_' + @name + 'OrderContext'') is not null begin
			drop trigger tr_' + @name + 'OrderContext
		end
		'

	exec(@sql)

	set @sql = '
		create trigger tr_' + @name + 'OrderContext  on [' + @name + '] after update, insert, delete
		as begin
			set nocount on
			declare @ExtenralContext_id bigint
			if (select ContextCaptureActive from [' + @dbp + ']..OrderContextConfig) = 1 begin
					
				update [' + @dbp + ']..OrderContextConfig set LastExternalContext = LastExternalContext + 1
				declare @ExternalContext_id bigint
				set @ExternalContext_id = (select LastExternalContext from [' + @dbp + ']..OrderContextConfig)
				

				if (select count(*) from inserted) > 0  and (select count(*) from deleted) > 0 begin 
					insert into [' + @dbp + ']..OrderContextLog (OrderContext_id, UsedTableName, UsedtableModfificationDate, UsedTableModificationType, ExternalContext_id, ContextSequence, ContextComment_id)
						select	(select OrderContext_id from [' + @dbp + ']..OrderContext where OrderContextActive = 1)
							,''' + @name + '''
							,getDate()
							,''update''
							,@ExternalContext_id
							,(select LastContextSequence from [' + @dbp + ']..OrderContext where OrderContextActive = 1)
							,(select ContextComment_id from [' + @dbp + ']..OrderContext where OrderContextActive = 1)
						from	inserted
					insert into [' + @dbp + ']..[' +@name + @tpostfix + ']
						select	d.*, 0, @ExternalContext_id from deleted d
					insert into [' + @dbp + ']..[' +@name + @tpostfix + ']
						select	i.*, 1, @ExternalContext_id from inserted i
	
				end
				else begin
					insert into [' + @dbp + ']..OrderContextLog (OrderContext_id, UsedTableName, UsedtableModfificationDate, UsedTableModificationType, ExternalContext_id, ContextSequence, ContextComment_id)
						select	(select OrderContext_id from [' + @dbp + ']..OrderContext where OrderContextActive = 1)
							,''' + @name + '''
							,getDate()
							,''insert''
							,@ExternalContext_id
							,(select LastContextSequence from [' + @dbp + ']..OrderContext where OrderContextActive = 1)
							,(select ContextComment_id from [' + @dbp + ']..OrderContext where OrderContextActive = 1)
						from	inserted
						union
						select	(select OrderContext_id from [' + @dbp + ']..OrderContext where OrderContextActive = 1)
							,''' + @name + '''
							,getDate()
							,''delete''
							,@ExternalContext_id
							,(select LastContextSequence from [' + @dbp + ']..OrderContext where OrderContextActive = 1)
							,(select ContextComment_id from [' + @dbp + ']..OrderContext where OrderContextActive = 1)
						from	deleted
					insert into [' + @dbp + ']..[' +@name + @tpostfix + ']
						select	d.*, 0, @ExternalContext_id from deleted d
					insert into [' + @dbp + ']..[' +@name + @tpostfix + ']
						select	i.*, 1, @ExternalContext_id from inserted i
				end
			end
		end'

	exec(@sql)
end

close TriggerAdd
deallocate TriggerAdd

go

---------------------------------------------------------------------------------------
-- STEP5: PROCEDURES
---------------------------------------------------------------------------------------
declare @db varchar(256)
declare @dbp varchar(256)
declare @postfix varchar(30)

set @postfix = (
	select	ConfigValue
	from	#scriptConfiguration
	where	Config_idn = 'DbNamePostfix'
	)
set @db = db_name()
set @dbp = @db + @postfix

exec('use ' + @dbp)
go

if object_id('showContext') is not null begin
	drop procedure showContext
end

go

create procedure showContext (
	@context_idn varchar(100)
	,@sequence int = null
	,@comment_idn [IDL] = null
	,@seqmode smallint = 0 -- 0 = all, 1 = last
	,@dateFrom datetime = '19000101'
	,@dateTo datetime = '99991231'
)
as begin

-- showContext 'default',null, null, 1 '20090101', '20090707'

	set nocount on

	if not exists (select 1 from OrderContext where OrderContext_idn = @context_idn) begin
		raiserror('Invalid Context', 16 , 1)
		return
	end

	if @seqmode is not null and @seqmode not in (0, 1) begin
		raiserror('Invalid mode', 16, 1)
		return
	end

	if @comment_idn is not null and not exists (select 1 from ContextComment where Comment_idn = @comment_idn) begin
		raiserror('Comment with given name not exists', 16 , 1)
		return
	end

	if @sequence is null and @seqmode = 0 begin
		set @sequence = null
	end
	else if @sequence is null and @seqmode = 1 begin
		set @sequence = (
			select	max(ContextSequence)
			from	OrderContextLog ocl
				join OrderContext oc on ocl.OrderContext_id = oc.OrderContext_id
					and oc.OrderContext_idn = @context_idn
			)
	end		

	declare showContext cursor for
		select	distinct
			ocl.UsedTableName
			,ocl.UsedtableModfificationDate
			,ocl.ExternalContext_id
			,ocl.UsedTableModificationType
			,cc.Comment	
		from	OrderContextLog ocl
			join OrderContext oc on ocl.OrderContext_id = oc.OrderContext_id
			left join ContextComment cc on ocl.ContextComment_id = cc.ContextComment_id
		where	oc.OrderContext_idn = @context_idn
			and UsedtableModfificationDate between @dateFrom and @dateTo
			and isnull(@sequence, ocl.ContextSequence) = ocl.ContextSequence
			and (ocl.ContextComment_id is null or isnull(@comment_idn, cc.Comment_idn) = cc.Comment_idn)
		order by
			ocl.UsedtableModfificationDate
			,ocl.ExternalContext_id

	open showContext

	declare @name varchar(256)
	declare @date datetime
	declare @row bigint
	declare @sql varchar(2000)
	declare @type varchar(15)
	declare @comment varchar(2000)
	declare @seq int
	set @seq = 1
	
	while 1=1 begin
		fetch next from showContext into @name, @date, @row, @type, @comment
		if @@fetch_status <> 0 begin
			break
		end
		set @sql = '
			select	''' + convert(varchar(10), @seq) + ''' as Seq 
				,''' + convert(varchar(50), @date, 121) + ''' as ModDate
				,''' + isnull(@type, '') + ''' as ModType
				,''' + isnull(@name, '') + ''' as TableName
				,t.*
				,''' + isnull(@comment, '') + ''' as Comment
			from	[' + @name + 'History] t
				join OrderContextLog ocl on t.ExternalContext_id = ocl.ExternalContext_id
					and ocl.ExternalContext_id = ' + convert(varchar(20), @row) + '
			'
		exec(@sql)
		set @seq = @seq + 1
	end

	close showContext
	deallocate showContext
end
go

if object_id('deleteContext') is not null begin
	drop procedure deleteContext
end
go

create procedure deleteContext (
	@context_idn varchar(100)
	,@sequence int = null
	,@comment_idn [IDL]= null
)
as begin

-- deleteContext 'default'

	set nocount on

	if not exists (select 1 from OrderContext where OrderContext_idn = @context_idn) begin
		raiserror('Invalid Context', 16 , 1)
		return
	end
	
	if @comment_idn is not null and not exists (select 1 from ContextComment where Comment_idn = @comment_idn) begin
		raiserror('Comment with given name not exists', 16 , 1)
		return
	end

	declare deleteContext cursor for
		select	distinct
			ocl.UsedTableName
			,ocl.ExternalContext_id
		from	OrderContextLog ocl
			join OrderContext oc on ocl.OrderContext_id = oc.OrderContext_id
			left join ContextComment cc on ocl.ContextComment_id = cc.ContextComment_id
		where	oc.OrderContext_idn = @context_idn
			and isnull(@sequence, ocl.ContextSequence) = ocl.ContextSequence
			and (ocl.ContextComment_id is null or isnull(@comment_idn, cc.Comment_idn) = cc.Comment_idn)
				
	open deleteContext

	declare @name varchar(256)
	declare @row bigint
	declare @sql varchar(2000)
	
	while 1=1 begin
		fetch next from deleteContext into @name, @row
		if @@fetch_status <> 0 begin
			break
		end
		set @sql = 'delete t from [' + @name + 'History] t where t.ExternalContext_id = ' + convert(varchar(20), @row)
		print @sql
		exec(@sql)

		set @sql = 'delete ocl from OrderContextLog ocl join OrderCOntext oc on oc.OrderContext_id = ocl.OrderContext_id where OrderContext_idn = ''' + @context_idn + ''' and ocl.ExternalContext_id = ' + convert(varchar(20), @row)
		print @sql
		exec(@sql)
		
	end

	if not exists (
		select	1
		from	OrderContextLog ocl
			join OrderContext oc on ocl.OrderContext_id = oc.OrderContext_id
				and oc.OrderContext_idn = @context_idn
	) begin
		set @sql = 'update OrderContext set LastContextSequence = 0 where OrderContext_idn = ''' + @context_idn + ''''
		print @sql
		exec (@sql)
	end
	
	close deleteContext
	deallocate deleteContext
end
go

if object_id('changeContext') is not null begin
	drop procedure changeContext
end
go

create procedure changeContext (
	@context_idn varchar(100)
	,@comment_idn [IDL] = null
	,@comment varchar(2000) = null
	,@update varchar(2) = 'N'
)
as begin
-- changeContext 'default'
	set nocount on

	if not exists (select 1 from OrderContext where OrderContext_idn = @context_idn) begin
		raiserror ('Invalid context', 16, 1)
		return
	end

	if ltrim(@comment_idn) = '' begin
		raiserror('Invalid comment name', 16, 1)
		return
	end

	if ltrim(@comment) = '' begin
		raiserror('Invalied comment', 16, 1)
		return
	end

	if @comment_idn is null and @comment is not null begin
		raiserror('Invalid combination of comment_idn and comment', 16, 1)
		return
	end

	if upper(@update) not in ('N', 'Y') begin
		raiserror('Incorrect update mode', 16, 1)
		return
	end

	declare @ContextComment_id [Id]
	set @ContextComment_id = null

	if @comment_idn is not null begin
		if @comment is not null
			and exists (select 1 from ContextComment where Comment_idn = @comment_idn) begin
			
			if @update = 'N' begin
				raiserror('Comment updates are forbidden in non update mode', 16, 1)
				return
			end
			else if @update = 'Y' begin
				update	ContextComment
				set	Comment = @comment
				where	Comment_idn = @comment_idn
			end
		end
		else if @comment is null
			and not exists (select 1 from ContextComment where Comment_idn = @comment_idn) begin

			raiserror('Comment with given idn not found', 16, 1)
			return
		end
		else if @comment is not null begin
			insert into ContextComment(Comment_idn, Comment) values (@comment_idn, @comment)
		end
		set @ContextComment_id = (select ContextComment_id from ContextComment where Comment_idn = @comment_idn)
	end
		
	update	c
	set	c.OrderContextActive = 0
	from	OrderContext c

	update	c
	set	c.OrderContextActive = 1
		,c.LastContextSequence = c.LastContextSequence + 1
		,c.ContextComment_id = @ContextComment_id
	from	OrderContext c
	where	c.OrderContext_idn = @context_idn
end

go
declare @db varchar(256)
set @db = db_name()

exec('use ' + @db)
go