if object_id('showContext') is not null begin
	drop procedure showContext
end
go

create procedure showContext (
	@context_idn varchar(100)
	,@sequence int = null
	,@comment_idn [IDL] = null
	,@seqmode smallint = 0 -- 0 = all, 1 = last
	,@dateFrom datetime = '19000101'
	,@dateTo datetime = '99991231'
)
as begin

-- showContext 'default',null, null, 1 '20090101', '20090707'

	set nocount on

	if not exists (select 1 from OrderContext where OrderContext_idn = @context_idn) begin
		raiserror('Invalid Context', 16 , 1)
		return
	end

	if @seqmode is not null and @seqmode not in (0, 1) begin
		raiserror('Invalid mode', 16, 1)
		return
	end

	if @comment_idn is not null and not exists (select 1 from ContextComment where Comment_idn = @comment_idn) begin
		raiserror('Comment with given name not exists', 16 , 1)
		return
	end

	if @sequence is null and @seqmode = 0 begin
		set @sequence = null
	end
	else if @sequence is null and @seqmode = 1 begin
		set @sequence = (
			select	max(ContextSequence)
			from	OrderContextLog ocl
				join OrderContext oc on ocl.OrderContext_id = oc.OrderContext_id
					and oc.OrderContext_idn = @context_idn
			)
	end		

	declare showContext cursor for
		select	distinct
			ocl.UsedTableName
			,ocl.UsedtableModfificationDate
			,ocl.ExternalContext_id
			,ocl.UsedTableModificationType
			,cc.Comment	
		from	OrderContextLog ocl
			join OrderContext oc on ocl.OrderContext_id = oc.OrderContext_id
			left join ContextComment cc on ocl.ContextComment_id = cc.ContextComment_id
		where	oc.OrderContext_idn = @context_idn
			and UsedtableModfificationDate between @dateFrom and @dateTo
			and isnull(@sequence, ocl.ContextSequence) = ocl.ContextSequence
			and (ocl.ContextComment_id is null or isnull(@comment_idn, cc.Comment_idn) = cc.Comment_idn)
		order by
			ocl.UsedtableModfificationDate
			,ocl.ExternalContext_id

	open showContext

	declare @name varchar(256)
	declare @date datetime
	declare @row bigint
	declare @sql varchar(2000)
	declare @type varchar(15)
	declare @comment varchar(2000)
	declare @seq int
	set @seq = 1
	
	while 1=1 begin
		fetch next from showContext into @name, @date, @row, @type, @comment
		if @@fetch_status <> 0 begin
			break
		end
		set @sql = '
			select	''' + convert(varchar(10), @seq) + ''' as Seq 
				,''' + convert(varchar(50), @date, 121) + ''' as ModDate
				,''' + isnull(@type, '') + ''' as ModType
				,''' + isnull(@name, '') + ''' as TableName
				,t.*
				,''' + isnull(@comment, '') + ''' as Comment
			from	[' + @name + 'History] t
				join OrderContextLog ocl on t.ExternalContext_id = ocl.ExternalContext_id
					and ocl.ExternalContext_id = ' + convert(varchar(20), @row) + '
			'
		exec(@sql)
		set @seq = @seq + 1
	end

	close showContext
	deallocate showContext
end
