create table OrderContextConfig (
	ContextCaptureActive smallint
	,LastExternalContext bigint
)


 
create table OrderContext (
	OrderContext_id [ID] identity(1, 1) not null primary key clustered
	,OrderContext_idn [IDL] not null
	,OrderContextActive smallint not null default 0
	,LastContextSequence int not null default 0
	,ContextComment_id [ID] foreign key references ContextComment(ContextComment_id)

)

create table OrderContextLog (
	OrderContextLog_id [ID] identity(1, 1) not null primary key clustered
	,OrderContext_id [ID] not null foreign key references OrderContext(OrderContext_id)
	,UsedTableName sysname not null
	,UsedTableRow varbinary(8)
	,UsedtableModfificationDate datetime not null
	,UsedTableModificationType varchar(15) not null
	,ExternalContext_id bigint
	,ContextSequence int not null
	,ContextComment_id [ID] foreign key references ContextComment(ContextComment_id)
)


create table ContextComment (
	ContextComment_id [ID] identity(1, 1) not null primary key clustered
	,Comment_idn [IDL] not null
	,Comment varchar(2000)
)
-- drop table ForeignKeysTempHistory
-- drop table OrderContextLog

insert into OrderContextConfig (ContextCaptureActive, LastExternalContext)  values (1, 1)
insert into OrderContext (OrderContext_idn,OrderContextActive) values ('default', 1)
insert into OrderContext(OrderContext_idn, OrderContextActive) values ('user_login', 0)
insert into OrderContext(OrderContext_idn, OrderContextActive) values ('user_logout', 0)
insert into OrderContext(OrderContext_idn, OrderContextActive) values ('pre_process', 0)
insert into OrderContext(OrderContext_idn, OrderContextActive) values ('process', 0)
insert into OrderContext(OrderContext_idn, OrderContextActive) values ('dividend_calc', 0)

insert into OrderContext (OrderContext_idn, OrderContextActive)
	select	DocumentType_idn + '.maker'
		,1
	from	dev_gtas_lj_p42..DocumentType
	union
	select	DocumentType_idn + '.checker'
		,0
	from	dev_gtas_lj_p42..DocumentType
	order by
		1, 2

declare @name sysname
declare @sql varchar(8000)
declare @mark varchar(15)
declare @db sysname
set @mark = 'History'
set @db = 'dev_gtas_lj_p42_log'

declare TriggerAdd cursor for
	select	distinct
		o1.[oname]
	from	dbo.db_model_get_tables_columns o1
	where 	o1.oname not in (
		select	distinct
			oname
		from	dbo.db_model_get_tables_columns
		where	alterable = 'N' and cname <> 'TableRowVersion'
	)
for read only

open TriggerAdd

while 1=1 begin
	fetch next from TriggerAdd into @name

	if @@fetch_status <> 0 begin
		break
	end
	
	set @sql = '
-- 		if dbo.utl_columnExists(''' + @name + ''', ''TableRowVersion'') = 0 begin
-- 			alter table [' + @name + '] add [TableRowVersion] rowversion
-- 		end
		if object_id(''tr_' + @name + 'OrderContext'') is not null begin
			drop trigger tr_' + @name + 'OrderContext
		end

-- 		if dbo.utl_columnExists(''' + @name + ''', ''TableRowVersion'') = 1 begin
-- 			alter table [' + @name + '] drop column [TableRowVersion]
-- 		end
-- 
-- 		if dbo.utl_columnExists(''' + @name + ''', ''ContextOperationKind'') = 0 begin
-- 			alter table [' + @name + '] add [ContextOperationKind] smallint
-- 		end
-- 
-- 		if dbo.utl_columnExists(''' + @name + ''', ''ExternalContext_id'') = 0 begin
-- 			alter table [' + @name + '] add [ExternalContext_id] bigint
-- 		end

		'

	exec(@sql)

	set @sql = '
		create trigger tr_' + @name + 'OrderContext  on [' + @name + '] after update, insert, delete
		as begin
			set nocount on
			declare @ExtenralContext_id bigint
			if (select ContextCaptureActive from [' + @db + ']..OrderContextConfig) = 1 begin
					
				update [' + @db + ']..OrderContextConfig set LastExternalContext = LastExternalContext + 1
				declare @ExternalContext_id bigint
				set @ExternalContext_id = (select LastExternalContext from [' + @db + ']..OrderContextConfig)
				

				if (select count(*) from inserted) > 0  and (select count(*) from deleted) > 0 begin 
					insert into [' + @db + ']..OrderContextLog (OrderContext_id, UsedTableName, UsedTableRow, UsedtableModfificationDate, UsedTableModificationType, ExternalContext_id, ContextSequence, ContextComment_id)
						select	(select OrderContext_id from [' + @db + ']..OrderContext where OrderContextActive = 1)
							,''' + @name + '''
							,TableRowVersion
							,getDate()
							,''update''
							,@ExternalContext_id
							,(select LastContextSequence from [' + @db + ']..OrderContext where OrderContextActive = 1)
							,(select ContextComment_id from [' + @db + ']..OrderContext where OrderContextActive = 1)
						from	inserted
					insert into [' + @db + ']..[' +@name + @mark + ']
						select	d.*, 0, @ExternalContext_id from deleted d
					insert into [' + @db + ']..[' +@name + @mark + ']
						select	i.*, 1, @ExternalContext_id from inserted i
	
				end
				else begin
					insert into [' + @db + ']..OrderContextLog (OrderContext_id, UsedTableName, UsedTableRow, UsedtableModfificationDate, UsedTableModificationType, ExternalContext_id, ContextSequence, ContextComment_id)
						select	(select OrderContext_id from [' + @db + ']..OrderContext where OrderContextActive = 1)
							,''' + @name + '''
							,TableRowVersion
							,getDate()
							,''insert''
							,@ExternalContext_id
							,(select LastContextSequence from [' + @db + ']..OrderContext where OrderContextActive = 1)
							,(select ContextComment_id from [' + @db + ']..OrderContext where OrderContextActive = 1)
						from	inserted
						union
						select	(select OrderContext_id from [' + @db + ']..OrderContext where OrderContextActive = 1)
							,''' + @name + '''
							,TableRowVersion
							,getDate()
							,''delete''
							,@ExternalContext_id
							,(select LastContextSequence from [' + @db + ']..OrderContext where OrderContextActive = 1)
							,(select ContextComment_id from [' + @db + ']..OrderContext where OrderContextActive = 1)
						from	deleted
					insert into [' + @db + ']..[' +@name + @mark + ']
						select	d.*, 0, @ExternalContext_id from deleted d
					insert into [' + @db + ']..[' +@name + @mark + ']
						select	i.*, 1, @ExternalContext_id from inserted i
				end
			end
		end'

	exec(@sql)
end

close TriggerAdd
deallocate TriggerAdd

alter table OrderContext drop column TableRowVersion
alter table OrderContext drop column ContextOperationKind
alter table OrderContext drop column ExternalContext_id
alter table OrderContextLog drop column TableRowVersion
alter table OrderContextLog drop column ContextOperationKind
alter table OrderContextConfig drop column TableRowVersion
alter table OrderContextConfig drop column ContextOperationKind
alter table OrderContextConfig drop column ExternalContext_id
-- 
-- drop table ContextComment
declare timestampCols cursor for
	select	[Name]
	from	sysobjects  
	where	type = 'U'
		and name not in (
		'KeystoreHistory'
		,'OrderContext'
		,'DividendMessageLogHistory'
		,'OrderContextLog'
		,'OrderContextConfig'
		,'MessageHistory'
		,'DocumentContentHistory'
		,'dtpropertiesHistory'
		,'PermissionConstraintHistory'
		,'ForeignKeysTempHistory'
		,'GenParamColHistory'
		,'ExternalModuleConfOverrideHistory'
		)
for read only

declare @sql varchar(1000)
declare @name varchar(256)

declare @cname varchar(256)
declare @type varchar(100)
set @cname = 'TableRowVersion'
set @type = 'varbinary(8)'

open timestampCols

while 1=1 begin
	fetch next from timestampCols into @name
	if @@fetch_status <> 0 begin
		break
	end
	
	
	set @sql = '
		if dbo.utl_columnExists('''  + @name +  ''', ''' + @cname + ''') = 1 begin
			alter table [' + @name + '] drop column [' + @cname  + ']
		end'
	print @sql
	exec(@sql)

	set @sql = '
		if dbo.utl_columnExists('''  + @name +  ''', ''' + @cname + ''') = 0 begin
			alter table [' + @name + '] add  [' + @cname  + '] ' + @type + '
		end'

	print @sql
	exec(@sql)
end

close timestampCols
deallocate timestampCols


-- ExternalContext_id
-- ContextOperationKind

-- sp_helptext 'tr_FundGroupOrderContext'
-- insert into FundGroup (fundgroup_id) values (-1000)
-- select * from dev_gtas_lj_p42_log..fundgrouphistory
-- update  FundGroup set fundgroup_idn = 'hello' where fundgroup_id= -1000
-- delete from FundGroup where fundGroup_id = (-1000)
-- 
-- select * from sysobjects where type = 'tr'
-- 
-- select * from ordercontextlog
-- insert into lj_roadmap_log..ordercontext
-- 	select * from ordercontext
-- drop trigger tr_ordercontextlogOrderContext
-- AccountHistory
-- delete from ordercontextlog
-- 
-- select	o.name
-- from	sysobjects o
-- 	left join syscolumns c on c.id = o.id
-- 		and c.name like 'tablerowversion'
-- where	o.type = 'U'
-- 	and c.[id] is null 
-- 
-- 
-- if dbo.utl_columnExists('Keystore', 'TableRowVersion') = 0 begin
-- 			alter table [' + @name + '] add [TableRowVersion] rowversion
-- 		end
-- select dbo.utl_columnExists('Keystore', 'TableRowVersion')
--  select * from specificData
