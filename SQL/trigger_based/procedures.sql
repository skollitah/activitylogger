if object_id('showContext') is not null begin
	drop procedure dbo.showContext
end

go

create procedure dbo.showContext (
	@context_idn varchar(100)
	,@sequence int = null
	,@comment_idn [IDL] = null
	,@seqmode smallint = 0 -- 0 = all, 1 = last
	,@dateFrom datetime = '19000101'
	,@dateTo datetime = '99991231'
)
as begin

-- showContext 'default',null, null, 1 '20090101', '20090707'

	set nocount on

	if not exists (select 1 from OrderContext where OrderContext_idn = @context_idn) begin
		raiserror('Invalid Context', 16 , 1)
		return
	end

	if @seqmode is not null and @seqmode not in (0, 1) begin
		raiserror('Invalid mode', 16, 1)
		return
	end

	if @comment_idn is not null and not exists (select 1 from ContextComment where Comment_idn = @comment_idn) begin
		raiserror('Comment with given name not exists', 16 , 1)
		return
	end

	if @sequence is null and @seqmode = 0 begin
		set @sequence = null
	end
	else if @sequence is null and @seqmode = 1 begin
		set @sequence = (
			select	max(ContextSequence)
			from	OrderContextLog ocl
				join OrderContext oc on ocl.OrderContext_id = oc.OrderContext_id
					and oc.OrderContext_idn = @context_idn
			)
	end		

	declare showContext cursor for
		select	distinct
			ocl.UsedTableName
			,ocl.UsedtableModfificationDate
			,ocl.UsedTableModificationType
			,cc.Comment	
		from	OrderContextLog ocl
			join OrderContext oc on ocl.OrderContext_id = oc.OrderContext_id
			left join ContextComment cc on ocl.ContextComment_id = cc.ContextComment_id
		where	oc.OrderContext_idn = @context_idn
			and UsedtableModfificationDate between @dateFrom and @dateTo
			and isnull(@sequence, ocl.ContextSequence) = ocl.ContextSequence
			and (ocl.ContextComment_id is null or isnull(@comment_idn, cc.Comment_idn) = cc.Comment_idn)
		order by
			ocl.UsedtableModfificationDate

	open showContext

	declare @name varchar(256)
	declare @date datetime
	declare @sql varchar(2000)
	declare @type varchar(15)
	declare @comment varchar(2000)
	declare @seq int
	set @seq = 1
	
	while 1=1 begin
		fetch next from showContext into @name, @date, @type, @comment
		if @@fetch_status <> 0 begin
			break
		end
		set @sql = '
			select	
				DISTINCT
				''' + convert(varchar(10), @seq) + ''' as Seq 
				,''' + convert(varchar(50), @date, 121) + ''' as ModDate
				,''' + isnull(@type, '') + ''' as ModType
				,''' + isnull(@name, '') + ''' as TableName
				,t.*
				,''' + isnull(@comment, '') + ''' as Comment
			from	[' + @name + '] t
				join OrderContextLog ocl on t.UsedtableModfificationDate = ocl.UsedtableModfificationDate
					and ocl.UsedtableModfificationDate = ''' + convert(varchar(30), @date, 121) + '''
			'
		exec(@sql)
		set @seq = @seq + 1
	end

	close showContext
	deallocate showContext
end
go



if object_id('deleteContext') is not null begin
	drop procedure dbo.deleteContext
end
go

create procedure dbo.deleteContext (
	@context_idn varchar(100)
	,@sequence int = null
	,@comment_idn [IDL]= null
)
as begin

-- deleteContext 'default'

	set nocount on

	if not exists (select 1 from OrderContext where OrderContext_idn = @context_idn) begin
		raiserror('Invalid Context', 16 , 1)
		return
	end
	
	if @comment_idn is not null and not exists (select 1 from ContextComment where Comment_idn = @comment_idn) begin
		raiserror('Comment with given name not exists', 16 , 1)
		return
	end

	declare deleteContext cursor for
		select	distinct
			ocl.UsedTableName
			,ocl.UsedtableModfificationDate
		from	OrderContextLog ocl
			join OrderContext oc on ocl.OrderContext_id = oc.OrderContext_id
			left join ContextComment cc on ocl.ContextComment_id = cc.ContextComment_id
		where	oc.OrderContext_idn = @context_idn
			and isnull(@sequence, ocl.ContextSequence) = ocl.ContextSequence
			and (ocl.ContextComment_id is null or isnull(@comment_idn, cc.Comment_idn) = cc.Comment_idn)
				
	open deleteContext

	declare @name varchar(256)
	declare @date datetime
	declare @sql varchar(2000)
	
	while 1=1 begin
		fetch next from deleteContext into @name, @date
		if @@fetch_status <> 0 begin
			break
		end
		set @sql = 'delete t from [' + @name + '] t where t.UsedtableModfificationDate = ''' + convert(varchar(30), @date, 121) + ''''
		print @sql
		exec(@sql)

		set @sql = 'delete ocl from OrderContextLog ocl join OrderCOntext oc on oc.OrderContext_id = ocl.OrderContext_id where OrderContext_idn = ''' + @context_idn + ''' and ocl.UsedtableModfificationDate = ''' + convert(varchar(30), @date, 121) + ''''
		print @sql
		exec(@sql)
		
	end

	if not exists (
		select	1
		from	OrderContextLog ocl
			join OrderContext oc on ocl.OrderContext_id = oc.OrderContext_id
				and oc.OrderContext_idn = @context_idn
	) begin
		set @sql = 'update OrderContext set LastContextSequence = 0 where OrderContext_idn = ''' + @context_idn + ''''
		print @sql
		exec (@sql)
	end
	
	close deleteContext
	deallocate deleteContext
end
go

if object_id('changeContext') is not null begin
	drop procedure dbo.changeContext
end
go

create procedure dbo.changeContext (
	@context_idn varchar(100)
	,@comment_idn [IDL] = null
	,@comment varchar(2000) = null
	,@update varchar(2) = 'N'
)
as begin
-- changeContext 'default'
	set nocount on

	if not exists (select 1 from OrderContext where OrderContext_idn = @context_idn) begin
		raiserror ('Invalid context', 16, 1)
		return
	end

	if ltrim(@comment_idn) = '' begin
		raiserror('Invalid comment name', 16, 1)
		return
	end

	if ltrim(@comment) = '' begin
		raiserror('Invalied comment', 16, 1)
		return
	end

	if @comment_idn is null and @comment is not null begin
		raiserror('Invalid combination of comment_idn and comment', 16, 1)
		return
	end

	if upper(@update) not in ('N', 'Y') begin
		raiserror('Incorrect update mode', 16, 1)
		return
	end

	declare @ContextComment_id [Id]
	set @ContextComment_id = null

	if @comment_idn is not null begin
		if @comment is not null
			and exists (select 1 from ContextComment where Comment_idn = @comment_idn) begin
			
			if @update = 'N' begin
				raiserror('Comment updates are forbidden in non update mode', 16, 1)
				return
			end
			else if @update = 'Y' begin
				update	ContextComment
				set	Comment = @comment
				where	Comment_idn = @comment_idn
			end
		end
		else if @comment is null
			and not exists (select 1 from ContextComment where Comment_idn = @comment_idn) begin

			raiserror('Comment with given idn not found', 16, 1)
			return
		end
		else if @comment is not null begin
			insert into ContextComment(Comment_idn, Comment) values (@comment_idn, @comment)
		end
		set @ContextComment_id = (select ContextComment_id from ContextComment where Comment_idn = @comment_idn)
	end
		
	update	c
	set	c.OrderContextActive = 0
	from	OrderContext c

	update	c
	set	c.OrderContextActive = 1
		,c.LastContextSequence = c.LastContextSequence + 1
		,c.ContextComment_id = @ContextComment_id
	from	OrderContext c
	where	c.OrderContext_idn = @context_idn
end

go
