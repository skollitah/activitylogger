if object_id('changeContext') is not null begin
	drop procedure changeContext
end
go

create procedure changeContext (
	@context_idn varchar(100)
	,@comment_idn [IDL] = null
	,@comment varchar(2000) = null
	,@update varchar(2) = 'N'
)
as begin
-- changeContext 'default'
	set nocount on

	if not exists (select 1 from OrderContext where OrderContext_idn = @context_idn) begin
		raiserror ('Invalid context', 16, 1)
		return
	end

	if ltrim(@comment_idn) = '' begin
		raiserror('Invalid comment name', 16, 1)
		return
	end

	if ltrim(@comment) = '' begin
		raiserror('Invalied comment', 16, 1)
		return
	end

	if @comment_idn is null and @comment is not null begin
		raiserror('Invalid combination of comment_idn and comment', 16, 1)
		return
	end

	if upper(@update) not in ('N', 'Y') begin
		raiserror('Incorrect update mode', 16, 1)
		return
	end

	declare @ContextComment_id [Id]
	set @ContextComment_id = null

	if @comment_idn is not null begin
		if @comment is not null
			and exists (select 1 from ContextComment where Comment_idn = @comment_idn) begin
			
			if @update = 'N' begin
				raiserror('Comment updates are forbidden in non update mode', 16, 1)
				return
			end
			else if @update = 'Y' begin
				update	ContextComment
				set	Comment = @comment
				where	Comment_idn = @comment_idn
			end
		end
		else if @comment is null
			and not exists (select 1 from ContextComment where Comment_idn = @comment_idn) begin

			raiserror('Comment with given idn not found', 16, 1)
			return
		end
		else if @comment is not null begin
			insert into ContextComment(Comment_idn, Comment) values (@comment_idn, @comment)
		end
		set @ContextComment_id = (select ContextComment_id from ContextComment where Comment_idn = @comment_idn)
	end
		
	update	c
	set	c.OrderContextActive = 0
	from	OrderContext c

	update	c
	set	c.OrderContextActive = 1
		,c.LastContextSequence = c.LastContextSequence + 1
		,c.ContextComment_id = @ContextComment_id
	from	OrderContext c
	where	c.OrderContext_idn = @context_idn
end
